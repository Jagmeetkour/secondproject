# Generated by Django 3.0.4 on 2020-05-14 15:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('secondapp', '0018_order_processed_on'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='processed_on',
            field=models.DateTimeField(default=False),
        ),
    ]
