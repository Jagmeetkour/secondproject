# Generated by Django 3.0.4 on 2020-05-14 14:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('secondapp', '0015_auto_20200514_0700'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='processed_on',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
